package game;

import game.Sprites.Tierra;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    @FXML
    Canvas mainCanvas;
    public static int widthScreen, heightScreen;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        this.widthScreen = screenSize.width;
        this.heightScreen = screenSize.height;
        mainCanvas.setWidth(widthScreen);
        mainCanvas.setHeight(heightScreen);

        GraphicsContext graphicsContext = mainCanvas.getGraphicsContext2D();
        Tierra tierra = new Tierra(new Image("img/tierra.png"), 0,0);

        final long startNanoTime = System.nanoTime();

        new AnimationTimer() {
            @Override
            public void handle(long currentNanoTime) {
                double t = (currentNanoTime - startNanoTime) / 1000000000.0;
                tierra.setGraphicsContext(graphicsContext);
                tierra.clear();
                tierra.move(t);
                tierra.render();
            }
        }.start();

    }
}
