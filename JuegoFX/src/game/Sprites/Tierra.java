package game.Sprites;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Tierra extends Sprites{

    public Tierra(Image image, int x, int y) {
        super(image, x, y);
    }

    public Tierra(Image image, int width, int height, int x, int y) {
        super(image, width, height, x, y);
    }

    public void setGraphicsContext(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public void move(double t) {
        x = (232 + 128 * Math.cos(t));
        y = (232 + 128 * Math.sin(t));
    }
}
