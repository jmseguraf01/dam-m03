package game.Sprites;

import game.GameController;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Sprites {
    protected Image image;
    protected int width, height;
    protected double x;
    protected double y;
    protected GraphicsContext graphicsContext;

    public Sprites(Image image, int x, int y) {
        this.image = image;
        this.x = x;
        this.y = y;
    }

    public Sprites(Image image, int width, int height, int x, int y) {
        this.image = image;
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public void move(GraphicsContext graphicsContext) {
        return;
    }

    public void render() {
        graphicsContext.drawImage(image, x, y);
    }

    public void clear() {
        graphicsContext.clearRect(x, y, GameController.widthScreen, GameController.heightScreen);
    }
}
