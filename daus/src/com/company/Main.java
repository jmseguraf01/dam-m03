package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();

        System.out.print("Escribe cuantas veces quieres tirar los dados: ");
        int vecesTirar = scanner.nextInt();
        // Array de los numeros de los dados
        int[] numeros = new int[11];
        System.out.println("Lanzamiento...");

        for (int i = 0; i < vecesTirar; i++) {
            // Numeros random
            int dado1 = (int) Math.floor(Math.random()*6+1);
            int dado2 = (int) Math.floor(Math.random()*6+1);
            int numero = dado1 + dado2;
            System.out.println(numero);
            // Comparo todos los numeros para encontrar cual es el random
            for (int j = 2; j <= 12 ; j++) {
                // Si es el numero, lo busco en el array y le sumo
                if (numero == j) {
                    numeros[j-2]++;
                }
            }
        }

        // Imprimo el resultado
        System.out.println("Resultado:");
        for (int i = 2; i <= 12; i++) {
            System.out.println(i + " -->  " +  numeros[i-2] + " vegades");
        }

    }
}
