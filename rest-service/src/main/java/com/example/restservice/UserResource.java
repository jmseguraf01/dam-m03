package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(UserResource.USERS)
public class UserResource {
    public static final String USERS = "/users";

    UserController userController;

    @Autowired
    public UserResource(UserController userController) {
        this.userController = userController;
    }

    @GetMapping
    private List<UserDto> userList() {
        return userController.readAll();
    }

    @GetMapping("/{id}")
    private UserDto getUsers(@PathVariable String id) {
        return userController.getUserWithId(Integer.parseInt(id));
    }

    @GetMapping("/{id}/email")
    private Map<String, String> email(@PathVariable Integer id) {
        return Collections.singletonMap("email", userController.getEmailWithId(id));
    }

    @PostMapping
    private UserDto addUser(@RequestBody UserDto user) {
        return userController.addUser(user);
    }

    @DeleteMapping("/{id}")
    private UserDto deleteUser(@PathVariable String id) {
        return userController.deleteUser(Integer.parseInt(id));
    }

    @PutMapping("/{id}")
    private UserDto replaceUser(@RequestBody UserDto userDto, @PathVariable String id) {
        return userController.updateUser(userDto, Integer.parseInt(id));
    }

    @PatchMapping("/{id}")
    private UserDto replaceEmail( @RequestBody Map<String, Object> updates, @PathVariable("id") String id) {
        return userController.updateEmailUser(updates, Integer.parseInt(id));
    }

}
