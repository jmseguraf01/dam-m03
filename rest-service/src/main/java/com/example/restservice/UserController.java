package com.example.restservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class UserController {
    UserDao userDao;

    @Autowired
    public UserController(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<UserDto> readAll() {
        return userDao.findAll().stream().map(UserDto::new).collect(Collectors.toList());
    }

    public UserDto getUserWithId(int id) {
        Optional<User> user = userDao.findById(id);
        if (user.isPresent()) return new UserDto(user.get());
        else return null;
    }

    public String getEmailWithId(int id) {
        return getUserWithId(id).getEmail();
    }

    public UserDto addUser(UserDto userDto) {
        User user = new User(userDto);
        userDao.save(user);
        return userDto;
    }

    public UserDto deleteUser(int id) {
        User user = new User(getUserWithId(id));
        userDao.delete(user);
        return new UserDto(user);
    }

    public UserDto updateUser(UserDto userDto, int id) {
        User user = new User(userDto);
        return userDao.findById(id).map(u -> {
            u.setEmail(user.getEmail());
            u.setPassword(user.getPassword());
            u.setFullName(user.getFullName());
            userDao.save(u);
            return new UserDto(u);
        }).orElseGet(() -> {
            return new UserDto(userDao.save(user));
        });
    }

    public UserDto updateEmailUser(Map<String, Object> datos, int id) {
        User user = new User(getUserWithId(id));
        datos.forEach(
                (change, value) -> {
                    switch (change) {
                        case "email":
                            user.setEmail((String) value);
                            break;
                        case "password":
                            user.setPassword((String) value);
                            break;
                        case "fullname":
                            user.setFullName((String) value);
                            break;
                    }
                });
        userDao.save(user);
        return new UserDto(user);
    }
}
