package com.company;

import java.util.Scanner;

public class Menu {
    Scanner scanner = new Scanner(System.in);
    int partidasTotal = 0;
    int partidasGanadas = 0;
    int partidasPerdidas = 0;
    int opcion;

    public void start() {
        do {
            opcion = mostrarMenu();
            switch (opcion) {
                case 1:
                    JocDaus jocDaus = new JocDaus();
                    jocDaus.jugar();
                    System.out.println(jocDaus);
                    // Compruebo si ha ganado
                    boolean hasGando = jocDaus.comprobarHasGanado();
                    if (hasGando) {
                        partidasGanadas++;
                        System.out.println("HAS GANADO!!");
                        mostrarPartidas();
                    } else {
                        partidasPerdidas++;
                    }
                    partidasTotal++;
                    break;

                case 2:

                case 3:
                    System.out.println("Aun no esta disponible este juego.");
                    break;

                    // Exit
                case 0:
                    mostrarPartidas();
                    break;
            }
        } while (opcion != 0);
    }

    private void mostrarPartidas() {
        System.out.println("Estadisticas: ");
        System.out.println(" - Partidas jugadas: " + partidasTotal);
        System.out.println(" - Partidas ganadas: " + partidasGanadas);
        System.out.println(" - Partidas perdidas: " + partidasPerdidas);
        System.exit(0);
    }

    // Muestra menu y devuelve la opcion escogida
    private int mostrarMenu() {
        System.out.println("-------------");
        System.out.println("  G A M E S  ");
        System.out.println( "1. Dados");
        System.out.println( "2. Parxis");
        System.out.println( "3. Fifa");
        System.out.println( "0. Exit");
        System.out.println("-------------");
        return scanner.nextInt();
    }
}
