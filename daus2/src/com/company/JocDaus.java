package com.company;



public class JocDaus {
    Dado[] dados = new Dado[3];

    @Override
    public String toString() {
        return "JocDaus{" +
                "dados=[Dau]{valor=" + dados[0].getValor() + "},[Dau]{valor=" + dados[1].getValor()
                + "},[Dau]{valor=" + dados[2].getValor() + '}';
    }

    // Creo la clase de dados
    public JocDaus() {
        for (int i = 0; i < 3; i++) {
            dados[i] = new Dado();
        }
    }

    // Tiro los dados y actualizo la variable "valor" del dado
    public void jugar() {
        for (int i = 0; i < 3; i++) {
            dados[i].setValor(dados[i].tirar());
        }
    }

    public boolean comprobarHasGanado() {
        boolean hasGanado = false;
        if (dados[0].getValor() == dados[1].getValor() && dados[1].getValor() == dados[2].getValor()) {
            hasGanado = true;
        }
        return hasGanado;
    }

}
