import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void imprimir(Persona persona) {
        System.out.println("<=================>");
        System.out.println(persona);
        System.out.println("<=================>");
    }

    public static void main(String[] args) {
        // Ej 1
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        // Ej 2
        Persona p1 = new Persona("Arya", Persona.Genere.DONA, LocalDate.parse("25/12/2002",format));
        Persona p2 = new Persona("Tyrion", Persona.Genere.HOME, LocalDate.parse("12/10/1980",format));
        Persona p3 = new Persona("Cersei", Persona.Genere.DONA, LocalDate.parse("10/01/1984",format));
        Persona p4 = new Persona("Eddard", Persona.Genere.HOME, LocalDate.parse("24/04/1974",format));
        Persona p5 = new Persona("Sansa", Persona.Genere.DONA, LocalDate.parse("24/04/1992",format));
        Persona p6 = new Persona("Jaime", Persona.Genere.HOME, LocalDate.parse("24/04/1979",format));
        Persona p7 = new Persona("Khal", Persona.Genere.HOME, LocalDate.parse("10/08/1979",format));
        Persona p8 = new Persona("Daenerys", Persona.Genere.DONA, LocalDate.parse("12/11/1992",format));
        Persona p9 = new Persona("Davos", Persona.Genere.HOME, LocalDate.parse("12/11/1965",format));
        Persona p10 = new Persona("Jon Neu", Persona.Genere.HOME, LocalDate.parse("12/11/1986",format));
        Persona p11 = new Persona("Brienne", Persona.Genere.DONA, LocalDate.parse("12/11/1989",format));
        // Ej 3
        List<Persona> personaList = new ArrayList<>();
        personaList.add(p1);
        personaList.add(p2);
        personaList.add(p3);
        personaList.add(p4);
        personaList.add(p5);
        personaList.add(p6);
        personaList.add(p7);
        personaList.add(p8);
        personaList.add(p9);
        personaList.add(p10);
        personaList.add(p11);


        // Labmda expresion - Ej 4
        Collections.sort( personaList, (o1, o2) -> {
            if (o1.getNom().charAt(0) >= o2.getNom().charAt(0)) return 1;
            else return -1;
        });

        // For en lambda expression - Ej 5
        personaList.forEach(persona -> System.out.println(persona));


        // De lamba a codigo normal - Ej 6
//        llistaPersones.sort((o1,o2) -> o2.getNom().compareTo(o1.getNom()));
        Collections.sort(personaList, new Comparator<Persona>() {
            @Override
            public int compare(Persona o1, Persona o2) {
                return o2.getNom().compareTo(o1.getNom());
            }
        });


        // Ej 7
        personaList.forEach(Main::imprimir);

        // Ej 8
        Map<Integer, Integer> mapPerson = new HashMap<>();
        personaList.forEach(persona -> {
            // Agrego los datos a la lista, siempre que no existan
            mapPerson.putIfAbsent(persona.getAge(), 0);
            // Si existen los datos, le cambio el value a value + 1
            mapPerson.computeIfPresent(persona.getAge(),(key ,value) -> value + 1);
        });


        // Ej 9
        mapPerson.forEach((key,value) -> System.out.println("Age: " + key + " - Veces: " + value));

        // Ej 10 - Personas que son de genero dona
        List<Persona> donasStream = personaList.stream().filter(persona -> persona.getGenere().equals(Persona.Genere.DONA)).collect(Collectors.toList());

        // Ej 11 - Personas mas pequeñas que 25
        List<Persona> menors25Stream = personaList.stream().filter(persona -> persona.getAge() < 25).collect(Collectors.toList());

        // Ej 12 - personas con A en su nombre
        List<Persona> personasConA = personaList.stream().filter(persona -> persona.getNom().toLowerCase().contains("a")).collect(Collectors.toList());

        // Ej 13 - los dos hombres mas jovenes
        List<Persona> masJovenes = personaList.stream().sorted(((o1, o2) -> {
            if (o1.getAge() > o2.getAge()) return 1;
            else if (o1.getAge() < o2.getAge()) return -1;
            else return 0;
        })).limit(2).collect(Collectors.toList());

        // Ej 14 borrar listado de personas entre 30 y 40
        personaList.removeIf(persona -> (persona.getAge() > 29 && persona.getAge() < 41) );

        // Ej 15 - con el map devolvemos las fechas + 2 que luego imprimimos
        personaList.stream().map(persona -> persona.getDataNaixament().plusDays(2)).forEach(System.out::println);

        // Ej 16
        List<LocalDate> personasRejovenidas = personaList.stream().map(persona -> persona.getDataNaixament().minusYears(2)).collect(Collectors.toList());
        for (int i = 0; i < personaList.size(); i++) {
            personaList.get(i).setDataNaixament(personasRejovenidas.get(i));
        }

        personaList.forEach(persona -> System.out.println(persona.getDataNaixament() + " " + persona.getNom()));

    }
}
