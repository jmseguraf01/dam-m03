package sample.Menu;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import sample.Game.GameController;
import sample.Jugador.Jugador;
import sample.Windows.MainWindows;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class MenuController implements Initializable {

    @FXML
    private TextField textFieldNombre;

    // Abro la nueva ventana de jugar
    public void clickJugar(ActionEvent event) throws IOException {
        // Cuando se haya escrito un nombre
        if (!textFieldNombre.getText().isBlank()) {
            actualizarJugadorActual();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../Game/game.fxml"));
            Parent root = loader.load();
            // Actualizo la scena y la cargo
            MainWindows.scene = new Scene(root);
            MainWindows.stage.setScene(MainWindows.scene);
            MainWindows.stage.setTitle("JUGAR");
            // Le añado el tema favorito
            MainWindows.scene.getStylesheets().add(MainWindows.temaActual);
            MainWindows.stage.setResizable(false);
            MainWindows.stage.show();
        }
        // El nombre esta en blanco
        else {
            textFieldNombre.setStyle("-fx-text-box-border: #B22222; -fx-focus-color: #B22222;");
        }
    }

    // Cerrar
    public void clickSalir(ActionEvent event) {
        Platform.exit();
    }

    // Actualizo el jugador actual de la partida
    private void actualizarJugadorActual() {
        GameController.jugadorActual = new Jugador(textFieldNombre.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
