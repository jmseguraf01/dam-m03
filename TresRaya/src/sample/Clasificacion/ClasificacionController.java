package sample.Clasificacion;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import sample.Game.Game;
import sample.Game.GameController;
import sample.Jugador.Jugador;
import sample.Menu.MenuController;

import java.net.URL;
import java.util.ResourceBundle;

public class ClasificacionController implements Initializable {
    private String[] resultados = {"Partidas ganadas", "Partidas empatadas", "Partidas perdidas"};
    @FXML
    private VBox vboxList;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Por cada jugador muestro los resultados
        for (Jugador jugador : GameController.jugadorList) {
            HBox hBox = new HBox(3);
            hBox.getStyleClass().add("hboxStyle");
            for (int i = 0; i < resultados.length; i++) {
                Label labelTexto = new Label(resultados[i] + ": " + String.valueOf(jugador.getResultados(i)));
                labelTexto.getStyleClass().add("labelResultados");
                labelTexto.getStyleClass().add("labelResultado"+i);
//                Label labelResultado = new Label(String.valueOf(jugador.getResultados(i)));
                hBox.getChildren().add(labelTexto);
//                hBox.getChildren().addAll(labelTexto, labelResultado);
            }
            TitledPane titledPane = new TitledPane(jugador.getNombre(), hBox);
            vboxList.getChildren().add(titledPane);
        }
    }
}
