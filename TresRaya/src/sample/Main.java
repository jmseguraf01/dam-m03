package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Menu.MenuController;
import sample.Windows.MainWindows;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu/sample.fxml"));
        Parent root = loader.load();

        primaryStage.setTitle("Tres en raya");
        primaryStage.setResizable(false);

        MainWindows.scene = new Scene(root);
        MainWindows.stage = primaryStage;
        MainWindows.scene.getStylesheets().add(MainWindows.temaActual);
        primaryStage.setScene(MainWindows.scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
