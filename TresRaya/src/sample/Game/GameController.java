package sample.Game;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import sample.Jugador.Jugador;
import sample.Menu.MenuController;
import sample.Windows.MainWindows;

import java.io.IOException;
import java.net.URL;
import java.util.*;


public class GameController implements Initializable {
    @FXML
    private Label labelJugador;
    @FXML
    private RadioButton radioButton1;
    @FXML
    private RadioButton radioButton2;
    @FXML
    private RadioButton radioButton3;
    @FXML
    private GridPane panelButtons;
    @FXML
    private Button buttonStart;
    @FXML
    private ToggleGroup escoger;

    Game game;
    static public List<Jugador> jugadorList = new ArrayList<>();
    public static Jugador jugadorActual;
    private final String[] textRadioButton = {"vs Ordenador", "Ordenador vs Ordenador", "vs Jugador 2"};
    // Creo un array de radioButtons
    private RadioButton[] radioButtons;


    // Meotodo que se inicia el iniciar la ventana
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // Añado el jugador a la lista
        jugadorList.add(jugadorActual);
        // Actualizo la label del nombre
        labelJugador.setText(labelJugador.getText() + jugadorActual.getNombre());
        // Actualizo el texto de los radiobutton
        textRadioButton[0] = jugadorActual.getNombre() + " " + textRadioButton[0];
        textRadioButton[2] = jugadorActual.getNombre() + " " + textRadioButton[2];
        // Inicio el array de radiobuttons
        radioButtons = new RadioButton[] {radioButton1, radioButton2, radioButton3};
        // Actualizo el texto de los radiobuttons
        for (int i = 0; i < radioButtons.length; i++) {
            radioButtons[i].setText(textRadioButton[i]);
        }
    }

    // Click en el boton start
    public void clickStart(ActionEvent actionEvent) {
        // Obtengo el checkbutton clicado
        RadioButton selectedRadioButton = (RadioButton) escoger.getSelectedToggle();
        // Siempre que haya algun checkbuton marcado
        if (selectedRadioButton != null) {
            String id = selectedRadioButton.getId();
            int modoJuego = 0;
            for (int i = 0; i < radioButtons.length; i++) {
                // Si el boton clicado es ese, pongo el modo de juego
                if (id.equals(radioButtons[i].getId())) {
                    modoJuego = i + 1;
                }
            }
            // Creo el juego
            game = new Game(modoJuego, this);
            // Relleno los botones
            for (Node button : panelButtons.getChildren()) {
                game.buttons.add((Button) button);
            }
            game.habilitarBotones();
            // Reseteo el texto de los botones
            restartButtons();
            buttonStart.setVisible(false);
            // Modo ordenador vs ordenador
            if (modoJuego == 2) {
                game.clickButton(null);
            }
        }
    }


    // Click en el tres en ralla
    public void clickButton(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        if (!game.comprobarBotonPulsado(button)) {
            game.clickButton(button);
        }
    }

    // Restablece el texo de los botones
    private void restartButtons() {
        for (Button button : game.buttons) {
            button.setText(null);
            // Borro si alguno tiene la clase de ganador
            button.getStyleClass().removeAll("buttonsGanadores");
            button.getStyleClass().add("buttonsTresRaya");
        }
    }

    // Restablece el juego
    void restartGame() {
        game.deshabilitarBotones();
        buttonStart.setVisible(true);
    }

    // Abrir clasificacion
    public void abrirClasificacion(ActionEvent actionEvent) throws IOException {
        // Creo la nueva ventana de clasificacion y la abro
        Parent root = FXMLLoader.load(getClass().getResource("../Clasificacion/clasificacion.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(MainWindows.temaActual);
        stage.setScene(scene);
        stage.setTitle("Clasificacion");
        stage.setResizable(false);
        stage.show();
    }

    // Cambiar tema
    public void cambiarTema(ActionEvent actionEvent) {
        MenuItem item = (MenuItem) actionEvent.getSource();
        // Tema claro
        if (item.getId().equals("claro")) {
            MainWindows.scene.getStylesheets().remove("styles_dark.css");
            MainWindows.temaActual = "styles.css";
        }
        // Tema oscuro
        else if (item.getId().equals("oscuro")) {
            MainWindows.scene.getStylesheets().remove("styles.css");
            MainWindows.temaActual = "styles_dark.css";
        }
        // Añado el estilo
        MainWindows.scene.getStylesheets().add(MainWindows.temaActual);
    }

    // Cambiar usuario
    public void cambiarUsuario(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../Menu/sample.fxml"));
        Parent root = loader.load();
        // Actualizo la scena del menu y la cargo
        MainWindows.scene = new Scene(root);
        MainWindows.scene.getStylesheets().add(MainWindows.temaActual);
        MainWindows.stage.setScene(MainWindows.scene);
        MainWindows.stage.setTitle("Tres en raya");
        MainWindows.stage.setResizable(false);
    }

    // Exit
    public void exit(ActionEvent actionEvent) {
        new MenuController().clickSalir(actionEvent);
    }

}

