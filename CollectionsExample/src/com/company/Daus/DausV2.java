package com.company.Daus;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DausV2 {

    static private Map<Integer,Integer> dausMap = new HashMap<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Cuantas veecs quieres tirar los dados?");
        int vecesTirar = scanner.nextInt();
        // Relleno la clave
        for (int i = 2; i < 13; i++) {
            dausMap.put(i , 0);
        }

        // Por cada vez que tiras
        for (int i = 0; i < vecesTirar; i++) {
            int dado0 = (int) Math.floor(Math.random()*6+1);
            int dado1 = (int) Math.floor(Math.random()*6+1);
            int resultado = dado0 + dado1;
            // Cojo la key de cada entrada
            for (Integer key : dausMap.keySet()) {
                // Si la key es igual al resultado le añado mas 1 al valor
                if (key == resultado) {
                    dausMap.put(key, dausMap.get(key) + 1 );
                }
            }
        }

        // Imprimo los resultados
        for (Integer key: dausMap.keySet()){
            System.out.println("Key: " + key.toString() + " / Value: " + dausMap.get(key));
        }

    }
}
