package com.company.Daus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    static private ArrayList<Daus> dausArrayList = new ArrayList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numDados = 2;
        System.out.println("Cuantas veces deseas tirar los dados? ");
        int vecesTirar = scanner.nextInt();

        // Por cada vez que tiras
        for (int i = 0; i < vecesTirar; i++) {
            // Por los dos dados que se tiran cada vez
            tirarDado();
        }

        // Miro cuantas veces se repite el numero de dados
        for (int i = 2; i < 13; i++) {
            System.out.println(i + " ha salido " + Collections.frequency(dausArrayList, new Daus(i)) + " veces.");
        }


    }

    private static void tirarDado() {
        int dado0 = (int) Math.floor(Math.random()*6+1);
        int dado1 = (int) Math.floor(Math.random()*6+1);
        dausArrayList.add(new Daus(dado0 + dado1));
    }
}
