package com.company.Daus;

import java.util.Objects;

public class Daus {
    int numero;

    public Daus(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Daus{" +
                "numero=" + numero +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Daus daus = (Daus) o;
        return numero == daus.numero;
    }
}
