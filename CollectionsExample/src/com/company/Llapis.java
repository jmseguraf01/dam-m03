package com.company;

import java.util.Objects;

public class Llapis {
    int color;
    float gruix;

    // Para que pueda compararlos y saber cuando son iguales los valores
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Llapis llapis = (Llapis) o;
        return color == llapis.color &&
                Float.compare(llapis.gruix, gruix) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, gruix);
    }

    // Constructor vacio
    public Llapis() {
        color = 0;
        gruix = 0;
    }

    // Constructor cuando pasas solo el color
    public Llapis(int color) {
        this.color = color;
        this.gruix = 0;
    }

    // Constructor cuando pasas los dos parametros
    public Llapis(int color, float gruix) {
        this.color = color;
        this.gruix = gruix;
    }

    @Override
    public String toString() {
        return "Llapis{" +
                "color=" + color +
                ", gruix=" + gruix +
                '}';
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public float getGruix() {
        return gruix;
    }

    public void setGruix(float gruix) {
        this.gruix = gruix;
    }
}
