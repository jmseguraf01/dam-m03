package com.company;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Map<Integer, String> map_colors = new HashMap<>();
        //Afegim entrades al map
        map_colors.put(0,"Negre");
        map_colors.put(1,"Vermell");
        map_colors.put(2,"Groc");
        map_colors.put(3,"Verd");
        map_colors.put(4,"Verd");
        map_colors.put(3,"Blanc");

        // Explica què passa amb el color verd i amb la clau 3. Perquè?
        //  - Lo que pasa es que la clave 3 mas tarde se sobreescribe por el color blanco

        // Imprimeix per pantalla, utilitzant un bucle for, tota la imformació del Map (clau i valor)
        System.out.println("Key y valores:");
        for (Integer name: map_colors.keySet()){
            System.out.println("Key: " + name.toString() + " / Value: " + map_colors.get(name));
        }

        // Imprimeix per pantalla, utilitzant un bucle for, només els valors del Map
        System.out.println("\n\nValores: ");
        for (int i = 0; i < map_colors.size(); i++) {
            System.out.println(map_colors.get(i));
        }
    }
}
