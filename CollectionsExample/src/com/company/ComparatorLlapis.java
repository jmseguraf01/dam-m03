package com.company;

import java.util.Comparator;

// Clase que compara
public class ComparatorLlapis implements Comparator<Llapis> {
    @Override
    public int compare(Llapis l1, Llapis l2) {
        if (l1.getGruix() > l2.getGruix()) {
            return 1;
        } else if (l1.getGruix() < l2.getGruix()) {
            return -1;
        } else {
            return 0;
        }
    }

}
