package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Llapis> llapisList0 = new ArrayList<>();
        List<Llapis> llapisList1 = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            // 10 lapices con colores diferentes
            llapisList0.add(new Llapis( (int) (Math.random() * 10)));
            // 10 lapices con diferentes colores y diferentes gruixos
            llapisList1.add(new Llapis((int) (Math.random() * 10), (float) (Math.random() * 10)));
        }

        // Imprimo el contenido de las listas
        System.out.println("Lista 0: ");
        imprimirLista(llapisList0);
        System.out.println("\n\nLista 1: ");
        imprimirLista(llapisList1);

        // Ordeno de menor a mayor por colores
        Collections.sort(llapisList0, new Comparator<Llapis>() {
            @Override
            public int compare(Llapis llapis0, Llapis llapis1) {
                // Compruebo si es mas grande, mas pequeño o igual
                if (llapis0.getColor() > llapis1.getColor()) {
                    return 1;
                } else if (llapis0.getColor() < llapis1.getColor()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        /* Leyenda de la clase compare:
        - Si devuelve un num positivo mas grande
        - Si devuelve un num negativo mas pequeño
        - Si devuelve 0 es igual
        */

        // Para darle la vuelta (ordenarlo de mayor a menor)
        Collections.reverse(llapisList0);
        System.out.println("\n\nLista 0 ordenada de mayor a menor por colores: ");
        imprimirLista(llapisList0);

        // Ordeno los lapices por el gruix
        Collections.sort(llapisList1, new ComparatorLlapis());
        System.out.println("\n\nLista 1 ordenada de menor a mayor por gruix: ");
        imprimirLista(llapisList1);

        // Guardo los array en linkedist
        LinkedList<Llapis> linkedList = new LinkedList<>(llapisList1);
        System.out.println("\n\nLinkedlist con el contenido llapislist1:");
        imprimirLista(linkedList);
        // Añado al linkedlist la lista 0
        linkedList.addAll(llapisList0);
        System.out.println("\n\nLinkedlist con el contenido de los dos arrays:");
        imprimirLista(linkedList);

        // Lista con el contenido de linkedlist pero sin repetirse
        System.out.println("\n\nSin repetidos");
        System.out.println("-----------");
        // Solo te añade los que no se repiten, pero tienes que tener implementado el metodo equals en LLAPIS
        LinkedHashSet<Llapis> listaSinRepetidos = new LinkedHashSet<>(linkedList);
        // Imprimo la lista sin repetidos
        listaSinRepetidos.forEach(System.out :: println);
    }

    // Imprime la lista
    static void imprimirLista(List list) {
        list.forEach(System.out :: println);
    }

}
