package com.company;

public class Ingresar extends Thread {
    private int dinero;
    private Client client;

    @Override
    public void run() {
        ingresar();
    }

    public Ingresar(Client client, int dinero) {
        this.dinero = dinero;
        this.client = client;
    }

    void ingresar() {
        client.setSaldo(client.getSaldo() + dinero);
    }
}
