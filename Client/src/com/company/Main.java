package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main{

    public static void main(String[] args) {

        Client juanmi = new Client("juanmi", 10);
        Ingresar ingresarJuanmi = new Ingresar(juanmi, 10);
        Retirar retirarJuanmi = new Retirar(juanmi, 10);

        ingresarJuanmi.start();
        retirarJuanmi.start();


    }
}
