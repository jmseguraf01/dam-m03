package com.company;


import java.util.List;

public class Client {
    private String Nom;
    private List<Integer> carret;
    private int saldo;

    public Client(String nom, List<Integer> carret) {
        this.carret = carret;
        Nom = nom;
        this.carret = carret;
    }

    public Client(String nom, int saldo) {
        Nom = nom;
        this.saldo = saldo;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public List<Integer> getCarret() {
        return carret;
    }

    public void setCarret(List<Integer> carret) {
        this.carret = carret;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
}
