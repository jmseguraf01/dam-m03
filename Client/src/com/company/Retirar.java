package com.company;

public class Retirar extends Thread {
    private int dinero;
    private Client client;

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            retirar();
        }
    }

    public Retirar(Client client, int dinero) {
        this.dinero = dinero;
        this.client = client;
    }

    void retirar() {
        client.setSaldo(client.getSaldo() - dinero);
    }
}
