package com.company;

public class Cuenta {
    Client client;
    public Cuenta(Client client) {
        this.client = client;
    }

    public void ingresar(int dinero) {
        client.setSaldo(client.getSaldo() + dinero);
    }

    public void retirar(int dinero) {
        int saldo = client.getSaldo();
        if (saldo - dinero >= 0) {
            client.setSaldo(client.getSaldo() - dinero);
        } else {
            System.out.println("Saldo insuficiente.");
        }
    }
}
