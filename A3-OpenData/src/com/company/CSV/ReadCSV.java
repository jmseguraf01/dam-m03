package com.company.CSV;

import com.company.Mercados.Mercados;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReadCSV {

    public List<Mercados> read() {
        List<Mercados> mercadosList = new ArrayList<>();
        System.out.println("Leyendo CSV . . .");
        String pathCSV = "https://opendata-ajuntament.barcelona.cat/data/dataset/706e7507-7f84-480e-940d-23265bf5d853/resource/98b893c2-ac83-4f34-b40a-19a4911a066b/download";
        URL url = null;
        BufferedReader in = null;
        CSVReader reader = null;
        try {
            url = new URL(pathCSV);
            in = new BufferedReader(new InputStreamReader(url.openStream()));
            // Leo el fichero entero y lo leo
            reader = new CSVReader(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Lo parseo con el formato de la clase Mercados
        CsvToBean<Mercados> activitatCsvToBean = new CsvToBeanBuilder(reader).withType(Mercados.class).withIgnoreLeadingWhiteSpace(true).build();

        Iterator<Mercados> csvIterator = activitatCsvToBean.iterator();
        // Por cada linea, la guardo
        while(csvIterator.hasNext()) {
            Mercados mercado = csvIterator.next();
            mercadosList.add(mercado);
        }
        return mercadosList;
    }
}
