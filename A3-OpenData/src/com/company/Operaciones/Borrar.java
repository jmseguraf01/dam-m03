package com.company.Operaciones;

import com.company.Mercados.Mercados;
import com.company.Utils;

import java.util.List;
import java.util.Scanner;

public class Borrar {
    private Scanner scanner = new Scanner(System.in);

    public void menu(List<Mercados> mercadosList) {
        Utils.clear();
        System.out.println("ELIMINAR");
        System.out.println("--------");
        System.out.println("1. Eliminar todo el registro");
        System.out.println("2. Eliminar el barrio");
        System.out.println("3. Eliminar el distrito");
        System.out.println("4. Eliminar telefono");
        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();

        if (opcion == 1) {
            eliminarRegistro(mercadosList);
        } else if (opcion == 2) {
            eliminarBarrio(mercadosList);
        } else if (opcion == 3) {
            eliminarDistrito(mercadosList);
        } else if (opcion == 4) {
            eliminarTelefono(mercadosList);
        }

    }

    // Elimina el registro entero
    private void eliminarRegistro(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        mercadosList.remove(mercadosList.get(numero));
        System.out.println("Registro eliminado con exito");
    }

    // Elimina el barrio
    private void eliminarBarrio(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        mercadosList.get(numero).setBARRI(0);
        System.out.println("Barrio eliminado con exito.");
    }

    // Elimina el distrito
    private void eliminarDistrito(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        mercadosList.get(numero).setDISTRICTE(0);
        System.out.println("Distrito eliminado con exito.");
    }

    // Elimina el telefono
    private void eliminarTelefono(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        mercadosList.get(numero).setTELEFON(null);
        System.out.println("Telefono eliminado con exito.");
    }


    // Devuelve el numero del mercado que ha escogido el usuario
    private int obtenerMercado(List<Mercados> mercadosList) {
        // Imprimo todos los mercados
        for (int i = 0; i < mercadosList.size(); i++) {
            System.out.println(i + ": " + mercadosList.get(i));
        }
        System.out.print("Escribe el numero del mercado que deseas eliminar: ");
        int numero = scanner.nextInt();
        scanner.nextLine();
        // Mientras el numero no exista
        while (numero < 0 || numero >= mercadosList.size()) {
            System.out.println("Error. Ese numero no pertenece a ningun mercado.");
            System.out.print("Escribe el numero del mercado que deseas modificar: ");
            numero = scanner.nextInt();
            scanner.nextLine();
        }

        return numero;
    }

}
