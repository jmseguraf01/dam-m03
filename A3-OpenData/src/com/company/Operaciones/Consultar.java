package com.company.Operaciones;

import com.company.Mercados.Mercados;
import com.company.Utils;

import java.util.*;

public class Consultar {
    private Scanner scanner = new Scanner(System.in);

    // Menu principal consultar
    public void menu(List<Mercados> mercadosList) {
        boolean salir = false;
        while (!salir) {
            Utils.clear();

            System.out.println("CONSULTAR");
            System.out.println("---------");
            System.out.println("1. Consultar todos los datos");
            System.out.println("2. Consultar por distrito");
            System.out.println("3. Consultar por nombre de barrio");
            System.out.println("4. Consultar por telefono");
            System.out.println("0. Volver");

            System.out.print("Escoge una opcion: ");
            int opcion = scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    consultarTodos(mercadosList);
                    break;

                case 2:
                    consultarDistrito(mercadosList);
                    break;

                case 3:
                    consultarBarrio(mercadosList);
                    break;

                case 4:
                    consultarTelefono(mercadosList);
                    break;

                case 0:
                    salir = true;
                    break;
            }
        }
    }

    // Consultar todos los datos
    private void consultarTodos(List<Mercados> mercadosList) {
        System.out.println("\n1. Ordenar por mayor Longitud");
        System.out.println("2. Ordenar por menor Longitud");
        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();

        // Lista de longitud ordenada
        List<Double> longitudOrdenado = new ArrayList<>();
        for (Mercados mercados : mercadosList) {
            longitudOrdenado.add(mercados.getLONGITUD());
        }

        // De mas grande a mas pequeño
        if (opcion == 1) {
            mercadosList.stream().sorted().forEach(System.out::println);
        }

        // De mas pequeño a mas grande
        else if (opcion == 2) {
            mercadosList.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);
        }
    }

    // Consultar distritos
    private void consultarDistrito(List<Mercados> mercadosList) {
        System.out.println("\n1. Mostrar todos los mercados de un distrito");
        System.out.println("2. Contar los mercados de un distrito");
        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Escribe el distrito: ");
        int distrito = scanner.nextInt();

        // Mostrar todos los mercados
        if (opcion == 1) {
            mercadosList.stream().filter(mercados -> mercados.getDISTRICTE() == distrito).forEach(System.out::println);
        }
        // Contar los mercados
        else if (opcion == 2) {
            long numeroDistritos = mercadosList.stream().filter(mercados -> mercados.getDISTRICTE() == distrito).count();
            System.out.println("Hay " + numeroDistritos + " mercados en el distrito " + distrito + ".");
        }
    }

    // Consulta barrio
    private void consultarBarrio(List<Mercados> mercadosList) {
        Utils.clear();
        System.out.println("1. Mostrar todos los mercados del barrio");
        System.out.println("2. Mostrar el primer mercado de la lista que coincida con el barrio");
        System.out.println("3. Mostrar el ultimo mercado de la lista que coincida con el barrio");

        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Escribe el barrio: ");
        int barrio = scanner.nextInt();
        scanner.nextLine();

        // Mostrar todos
        if (opcion == 1) {
            mercadosList.stream().filter(mercados -> mercados.getBARRI() == barrio).forEach(System.out::println);
        }
        // Mostrar el primer mercado de la lista
        else if (opcion == 2) {
            System.out.println(mercadosList.stream().filter(mercados -> mercados.getBARRI() == barrio).findFirst());
        }
        // Mostrar el ultimo mercado de la lista
        else if (opcion == 3) {
            System.out.println(mercadosList.stream().filter(mercados -> mercados.getBARRI() == barrio).reduce((first, second) -> second));
        }
    }

    // Consultar por los telefonos
    private void consultarTelefono(List<Mercados> mercadosList) {
        Utils.clear();
        System.out.println("1. Mostrar el mercado con ese numero de telefono");
        System.out.println("2. Mostrar todos los mercados que coincidan con los caracteres del telefono");
        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Escribe el numero de telefono: ");
        String telefono = scanner.nextLine();

        // El mercado con ese numero de telefono
        if (opcion == 1) {
            mercadosList.stream().filter(mercados -> mercados.getTELEFON().equals(telefono)).forEach(System.out::println);
        }

        // Todos los mercados que coincidan con la cadena de caracteres del telf
        else if (opcion == 2) {
            mercadosList.stream().filter(mercados -> mercados.getTELEFON().contains(telefono)).forEach(System.out::println);
        }

    }

}
