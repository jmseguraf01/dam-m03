package com.company.Operaciones;

import com.company.Mercados.Mercados;
import com.company.Utils;

import java.util.List;
import java.util.Scanner;

public class Modificar {
    private Scanner scanner = new Scanner(System.in);
    public void menu(List<Mercados> mercadosList) {
        Utils.clear();
        System.out.println("MODIFICAR");
        System.out.println("---------");
        System.out.println("1. Modificar barrio");
        System.out.println("1. Modificar distrito");
        System.out.println("3. Modificar telefono");
        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();
        scanner.nextLine();

        if (opcion == 1) {
            modificarBarrio(mercadosList);
        } else if (opcion == 2) {
            modificarDistrito(mercadosList);
        } else if (opcion == 3) {
            modificarTelefono(mercadosList);
        }
    }

    // Modificar barrio
    private void modificarBarrio(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        System.out.print("Escribe el numero del nuevo barrio: ");
        int barrio = scanner.nextInt();
        scanner.nextLine();
        mercadosList.get(numero).setBARRI(barrio);
    }

    // Modificar distrito
    private void modificarDistrito(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        System.out.print("Escribe el numero del nuevo distrito: ");
        int distrito = scanner.nextInt();
        scanner.nextLine();
        mercadosList.get(numero).setDISTRICTE(distrito);
    }

    // Modificar telefono
    private void modificarTelefono(List<Mercados> mercadosList) {
        int numero = obtenerMercado(mercadosList);
        System.out.print("Escribe el numero de telefono: ");
        String telefono = scanner.nextLine();
        mercadosList.get(numero).setTELEFON(telefono);
    }

    // Devuelve el numero del mercado que ha escogido el usuario
    private int obtenerMercado(List<Mercados> mercadosList) {
        // Imprimo todos los mercados
        for (int i = 0; i < mercadosList.size(); i++) {
            System.out.println(i + ": " + mercadosList.get(i));
        }
        System.out.print("Escribe el numero del mercado que deseas modificar: ");
        int numero = scanner.nextInt();
        scanner.nextLine();
        // Mientras el numero no exista
        while (numero < 0 || numero >= mercadosList.size()) {
            System.out.println("Error. Ese numero no pertenece a ningun mercado.");
            System.out.print("Escribe el numero del mercado que deseas modificar: ");
            numero = scanner.nextInt();
            scanner.nextLine();
        }

        return numero;
    }

}
