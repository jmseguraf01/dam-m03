package com.company.Menu;

import com.company.Mercados.Mercados;
import com.company.Operaciones.Borrar;
import com.company.Operaciones.Consultar;
import com.company.Operaciones.Modificar;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    List<Mercados> mercadosList;

    public Menu(List<Mercados> mercadosList) {
        this.mercadosList = mercadosList;
    }

    public void menuPrincipal() {
        while (true) {
            System.out.println("\n     M E N U   P R I N C I P A L    ");
            System.out.println("-------------------------------------");
            System.out.println("\t1. Consultar datos");
            System.out.println("\t2. Modificar datos");
            System.out.println("\t3. Borrar datos");
            System.out.print("Escoge la opcion que deses: ");
            int opcion = scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    new Consultar().menu(mercadosList);
                    break;

                case 2:
                    new Modificar().menu(mercadosList);
                    break;

                case 3:
                    new Borrar().menu(mercadosList);
                    break;
            }
        }
    }
}
