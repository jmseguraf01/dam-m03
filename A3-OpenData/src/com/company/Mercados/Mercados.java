package com.company.Mercados;


import com.opencsv.bean.CsvBindByName;
import lombok.Data;


@Data
public class Mercados implements Comparable<Mercados> {
    @CsvBindByName
    String CODI_CAPA;
    @CsvBindByName
    String CAPA_GENERICA;
    @CsvBindByName
    String NOM_CAPA;
    @CsvBindByName
    double ED50_COORD_X;
    @CsvBindByName
    double ED50_COORD_Y;
    @CsvBindByName
    double ETRS89_COORD_X;
    @CsvBindByName
    double ETRS89_COORD_Y;
    @CsvBindByName
    double LONGITUD;
    @CsvBindByName
    double LATITUD;
    @CsvBindByName
    String EQUIPAMENT;
    @CsvBindByName
    int DISTRICTE;
    @CsvBindByName
    int BARRI;
    @CsvBindByName
    String NOM_DISTRICTE;
    @CsvBindByName
    String NOM_BARRI;
    @CsvBindByName
    String ADRECA;
    @CsvBindByName
    String TELEFON;

    // Comparador de longitud, por defecto muestra de mayor a menor
    @Override
    public int compareTo(Mercados o) {
        if(this.getLONGITUD() < o.getLONGITUD()) return 1;
        else if(this.getLONGITUD() > o.getLONGITUD()) return -1;
        else return 0;
    }
}