package com.company;

import com.company.CSV.ReadCSV;
import com.company.Menu.Menu;

public class Main {

    public static void main(String[] args) {
        new Menu(new ReadCSV().read()).menuPrincipal();
    }
}
